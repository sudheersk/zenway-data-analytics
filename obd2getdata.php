<?php
require_once 'required/dbconnectnew.php';
require_once 'required/jsonioheader.php';

$vehicleId = mysqli_real_escape_string($conn, $inputData->{'vID'});
$fv = mysqli_real_escape_string($conn, $inputData->{'fv'});
$fts= mysqli_real_escape_string($conn, $inputData->{'from'});
$tts= mysqli_real_escape_string($conn, $inputData->{'to'});
$fhrs=mysqli_real_escape_string($conn, $inputData->{'fhrs'});
$thrs=mysqli_real_escape_string($conn, $inputData->{'thrs'});
echo $fts;
echo $tts;

//$vehicleId='11235';
//$fv='v2.1';

// This is the data you want to pass to Python
//$data = array($vehicleId, $fv);
//$data = array($vehicleId);
//$pass=$vehicleId ."-" . $fv;
//$my_array = array($vehicleId,$fv);


// Execute the python script with the JSON data
//$result = shell_exec('python3 obd2VehicleScore.py ' . );
$result = exec("python3 obd2VehicleScore.py $vehicleId $fv $fts $tts $fhrs $thrs");
//$result = exec("python3 obd2VehicleScore.py $fts $tts");
//$result = shell_exec('python3 obd2VehicleScore.py ' . escapeshellarg($pass));

//$result = shell_exec('python3 obd2VehicleScore.py .$vehicleId .$fv');

// Decode the result
$resultData = json_decode($result, true);

checkVehicleID();

echo JSON_encode($outputData);

///////FUNCTIONS

function checkVehicleID() {
	global $conn;
	global $vehicleId;
        global $outputData;

	$stmt = $conn->prepare('SELECT vehicleId FROM vehicleInfo WHERE vehicleId = ?');
	$stmt->bind_param('s', $vehicleId);
	$stmt->execute();
	$stmt->store_result();
	

	if ($stmt->num_rows == 0) {
		$outputData['error'] = 'VehicleID is incorrect.';
	}
	else {
		getParameterData();
	}
	$stmt->free_result();
	$stmt->close();
}

function getParameterData() {
	global $conn;
	global $outputData;
	global $vehicleId;
			
			$q = 'SELECT * FROM vehicleInfo WHERE vehicleId = \''.$vehicleId.'\'  LIMIT 1';
			$result = $conn->query($q);	
  	                $outputData["result"] = $result->fetch_array(MYSQLI_ASSOC);
}


?>