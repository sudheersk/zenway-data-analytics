<?php
require_once 'required/jsonioheader.php';

$vehicleID = $inputData['vID'];
$controllers = $inputData['cnts'];

checkVehicleID();

echo JSON_encode($outputData);

///////FUNCTIONS

function checkVehicleID() {
	global $vehicleID;
	global $outputData;

	$url = 'http://localhost:9200/obd2new/vehicles/_search?q=vehicleID:'.$vehicleID;
 
    $ci = curl_init();
    prepareCURL($ci, 'GET', $url, NULL);
    $responseJSON = json_decode(curl_exec($ci), true);
    curl_close($ci);
    
    if ($responseJSON['hits']['total'] == 0) {
    	$outputData['error'] = 'VehicleID is incorrect.';
    }
    else {
    	saveParameterData();
    }
}

function saveParameterData() {
	global $vehicleID;
	global $controllers;
	
//	foreach ($controllers as $controller) {
		//$controllerSerialNumber = $controller['CSN'];
		$firmwareVersion = $controller['FV'];
		$categories = $controller['cats'];
		//foreach ($categories as $categoryName => $parameters) {
			$bulkJSONText = '';
			//foreach ($parameters as $thisParameter) {
				$thisJSON = array();
				foreach ($thisParameter as $key => $value) {
					switch ($key) {
						case 'RPM':
							$thisJSON[$key] = floatval($value / 100);
							break;
						case 'SPEED':
							$thisJSON[$key] = floatval($value / 100);
							break;
						case 'FUELECO':
							$thisJSON[$key] = floatval($value / 10);
							break;
						case 'THPOSITION':
							$thisJSON[$key] = floatval($value / 100);
							break;
						case 'COOLANT':
							$thisJSON[$key] = floatval($value / 100);
							break;
						default:
							$thisJSON[$key] = $value;
					}
				}
				$thisJSON['vID'] = $vehicleID;
				//$thisJSON['CSN'] = $controllerSerialNumber;
				$thisJSON['FV'] = $firmwareVersion;
				
				$thisIndexInfo = array();
				$thisIndexInfo['_index'] = 'obd2';
				$thisIndexInfo['_type'] = $categoryName.'Parameters';
				
				$thisActionInfo = array();
				$thisActionInfo['index'] = $thisIndexInfo;
				
				$bulkJSONText .= json_encode($thisActionInfo)."\n".json_encode($thisJSON)."\n";
			//}
			
			$url = 'http://localhost:9200/_bulk';
 
   			$ci = curl_init();
   			prepareCURL($ci, 'POST', $url, $bulkJSONText);
			   echo $bulkJSONText;
  			$response = curl_exec($ci);
   			curl_close($ci);
   			$jsonResponse = json_decode($response, true);
   			print_r($jsonResponse['error']);
	//	}
	//}
}

function prepareCURL($thisCI, $thisRequestType, $thisURL, $thisJSON) {
	curl_setopt($thisCI, CURLOPT_URL, $thisURL);
    curl_setopt($thisCI, CURLOPT_PORT, 9200);
    curl_setopt($thisCI, CURLOPT_TIMEOUT, 200);
    curl_setopt($thisCI, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($thisCI, CURLOPT_FORBID_REUSE, 0);
    curl_setopt($thisCI, CURLOPT_CUSTOMREQUEST, $thisRequestType);
    if ($thisJSON != NULL) {
    	curl_setopt($thisCI, CURLOPT_POSTFIELDS, $thisJSON);
    }
}
?>