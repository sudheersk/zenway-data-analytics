import pymysql
import requests
import sys, json
import pandas as pd
import numpy as np

# Open database connection
db = pymysql.connect("localhost","zenwaygateway","CVVDEx82uRPnnpGB","obd2")
# prepare a cursor object using cursor() method
cursor = db.cursor()
# execute SQL query using execute() method.
cursor.execute("SELECT VERSION()")
# Fetch a single row using fetchone() method.
data = cursor.fetchone()

print ("Database version : %s " % data)
#print('hello there')
#passed = sys.argv[1]
#val = passed.partition("-")
#vehId=val[0];
#fv=val[2];

#vehId='89891'
vehId='11235'
fv='V1.0'
j = {  
   "from":0,
   "size":1000,
   "query":{  
      "bool":{  
         "must":[  
            {  
               "match":{  
                  "vID":"11235"
               }
            },
            {  
               "range":{  
                  "ts":{  
                     "gt":"2017-06-04 13:58:13",
                     "lt":"2017-06-09 13:58:13"

                  }
               }
            }
         ]
      }
   }
}


url = 'http://localhost:9200/zenwaycloudobd2new/parameters/_search'

#url = 'http://localhost:9200/zenwaycloudobd2new/_search?q=vID:'+vehId+'&size=1000'
#+ '&size=100'
#url = 'http://localhost:9200/zenwaycloudobd2new/_search?size=20'  

########## Monitoring parameters #################
out=json.dumps(j)
print(out);
response = requests.get(url, data=out)
#response = requests.get(url)
#print(response.text)
d=response;
d1=json.loads(d.text)

df1=d1["hits"]["hits"]
df = pd.io.json.json_normalize(df1)
#print('vID',df['_source.vID'][0]);
print('Size',df.shape)

def RPMScore(sd1):
 if(sd1<300):RPM=0.95
 elif(sd1>300 and sd1<400):RPM=0.85
 elif(sd1>400 and sd1<500):RPM=0.7 
 else:RPM=0.5
 return RPM;
#print('Unique Veh ID',df['_source.vID'])
print('Standard Deviation of RPM',df['_source.es'].astype(float).std())
#print('Max of RPM',df['_source.es'].astype(float).max());
#print('Min of RPM',df['_source.es'].astype(float).min());
print('Mean of RPM',df['_source.es'].astype(float).mean());
RPMMax=df['_source.es'].astype(float).max()
RPMMin=df['_source.es'].astype(float).min()
RPMMean=df['_source.es'].astype(float).mean()
RPM=RPMScore(df['_source.es'].astype(float).std());

def SPEEDScore(sd1):
 if(sd1<8):SPEED=0.9
 elif(10>sd1>8):SPEED=0.8
 elif(12>sd1>10):SPEED=0.7
 else:SPEED=0.6
 return SPEED;
print('Standard Deviation of SPEED',df['_source.vs'].astype(float).std());
#print('Max of SPEED',df['_source.vs'].astype(float).max());
#print('Min of SPEED',df['_source.vs'].astype(float).min());
print('Mean of SPEED',df['_source.vs'].astype(float).mean());
SPEEDMax=df['_source.vs'].astype(float).max()
SPEEDMin=df['_source.vs'].astype(float).min()
SPEEDMean=df['_source.vs'].astype(float).mean()

SPEED=SPEEDScore(df['_source.vs'].astype(float).std());

def FUELECOScore(sd1):
 if(sd1<3):FUELECO=0.9
 elif(5>sd1>3):FUELECO=0.8
 elif(7>sd1>5):FUELECO=0.7
 else:FUELECO=0.5
 return FUELECO;
print('Standard Deviation of FUELECO',df['_source.fe'].astype(float).std());
#print('Max of FUELECO',df['_source.fe'].astype(float).max());
#print('Min of FUELECO',df['_source.fe'].astype(float).min());
print('Mean of FUELECO',df['_source.fe'].astype(float).mean());
FUELECOMax=df['_source.fe'].astype(float).max()
FUELECOMin=df['_source.fe'].astype(float).min()
FUELECOMean=df['_source.fe'].astype(float).mean()
FUELECO=FUELECOScore(df['_source.fe'].astype(float).std());


def THPOSITIONScore(sd1):
 if(sd1<10):THPOSITION=0.9
 elif(12>sd1>10):THPOSITION=0.8
 elif(15>sd1>12):THPOSITION=0.7
 else:THPOSITION=0.5
 return THPOSITION;
print('Standard Deviation of THPOSITION',df['_source.tp'].astype(float).std());
#print('Max of THPOSITION',df['_source.tp'].astype(float).max());
#print('Min of THPOSITION',df['_source.tp'].astype(float).min());
print('Mean of THPOSITION',df['_source.tp'].astype(float).mean());
THPOSITIONMax=df['_source.tp'].astype(float).max()
THPOSITIONMin=df['_source.tp'].astype(float).min()
THPOSITIONMean=df['_source.tp'].astype(float).mean()
THPOSITION=THPOSITIONScore(df['_source.tp'].astype(float).std());

def COOLANTScore(sd1):
 if(sd1<6):COOLANT=0.9
 elif(8>sd1>6):COOLANT=0.8
 elif(11>sd1>8):COOLANT=0.7
 else:COOLANT=0.6
 return COOLANT;
print('Standard Deviation of COOLANT',df['_source.ct'].astype(float).std());
#print('Max of COOLANT',df['_source.ct'].astype(float).max());
#print('Min of COOLANT',df['_source.ct'].astype(float).min());
print('Mean of COOLANT',df['_source.ct'].astype(float).mean());
COOLANTMax=df['_source.ct'].astype(float).max()
COOLANTMin=df['_source.ct'].astype(float).min()
COOLANTMean=df['_source.ct'].astype(float).mean()
COOLANT=COOLANTScore(df['_source.ct'].astype(float).std());

score = ((0.2 * RPM) + (0.2 * SPEED) + (0.2 * FUELECO) +(0.2 * THPOSITION)+  (0.2 * COOLANT))*1600
print('Score',score)
print(vehId)
q1="""SELECT `vehicleScore` FROM `vehicleInfo` WHERE `vehicleId`='{0}'""".format(vehId)
cursor.execute(q1);
count=cursor.rowcount
print('Rows',count)
if(count==1):
 q3="""UPDATE `vehicleInfo` SET `vehicleScore`={0}, `FV`='{1}', `rpmMax`={2}, `rpmMin`={3}, `rpmAve`={4}, `speedMax`={5}, `speedMin`={6}, `speedAve`={7}, `fuelEcoMax`={8}, `fuelEcoMin`={9}, `fuelEcoAve`={10}, `thPositionMax`={11}, `thPositionMin`={12}, `thPositionAve`={13}, `coolantMax`={14}, `coolantMin`={15}, `coolantAve`={16} WHERE `vehicleId`='{17}'""".format(score,fv,RPMMax,RPMMin,RPMMean,SPEEDMax,SPEEDMin,SPEEDMean,FUELECOMax,FUELECOMin,FUELECOMean,THPOSITIONMax,THPOSITIONMin,THPOSITIONMean,COOLANTMax,COOLANTMin,COOLANTMean,vehId)
 cursor.execute(q3);
else:
 q="""INSERT INTO `vehicleInfo`(`vehicleId`, `FV`, `vehicleScore`, `rpmMax`, `rpmMin`, `rpmAve`, `speedMax`, `speedMin`, `speedAve`, `fuelEcoMax`, `fuelEcoMin`, `fuelEcoAve`, `thPositionMax`, `thPositionMin`, `thPositionAve`, `coolantMax`, `coolantMin`, `coolantAve`) 
   VALUES ('{0}','{1}',{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17})""".format(vehId,fv,score,RPMMax,RPMMin,RPMMean,SPEEDMax,SPEEDMin,SPEEDMean,FUELECOMax,FUELECOMin,FUELECOMean,THPOSITIONMax,THPOSITIONMin,THPOSITIONMean,COOLANTMax,COOLANTMin,COOLANTMean)
 cursor.execute(q);

db.commit()
#df.dropna()
#df.fillna(0)
print('DF Size',df.shape[0])

#df = df[np.isfinite(df['_source.es'])]
#print(df['_source.es'])
countBrake=0
count2Brake=0
countAcc=0
count2Acc=0
for i in range(df.shape[0]-2):
#for i in range(3,8):
  #print(df['_source.es'][i].astype(float))
 if(df['_source.es'][i].astype(float) != 0):
  change=((df['_source.es'][i].astype(float)-df['_source.es'][i+1].astype(float))/df['_source.es'][i].astype(float))
  if(change>0):
   #print('Change',change)
   if(change>0.4):
    if(df['_source.es'][i+1].astype(float) != 0):
     change2=((df['_source.es'][i+1].astype(float)-df['_source.es'][i+2].astype(float))/df['_source.es'][i+1].astype(float))
     #print('Change2',change2)
     if(change2>0.2):
      count2Brake=count2Brake+1
     countBrake=countBrake+1
  #print('Sudden braking')
   #print('time:',df['_source.ts'][i])
  if(change<0):
   #print('change',change)
   if(change<-0.4):
    countAcc=countAcc+1
    #print('acceleration')
    if(df['_source.es'][i+1].astype(float) != 0):
     change2=((df['_source.es'][i+1].astype(float)-df['_source.es'][i+2].astype(float))/df['_source.es'][i+1].astype(float))
     if(change2<-0.2):
      count2Acc=count2Acc+1
     #print('time:',df['_source.ts'][i])  
print('Count of sudden braking',countBrake)
print('Count2 of sudden braking',count2Brake)
print('Count of Acceleration',countAcc)
print('Count2 of Acceleration',count2Acc)
throttleAcc=0
for i in range(df.shape[0]-2):
 if(df['_source.es'][i].astype(float) != 0):
  thr=((df['_source.tp'][i].astype(float)-df['_source.tp'][i+1].astype(float))/df['_source.tp'][i].astype(float))
  if(thr>0):
   #print('throttle vary',thr)
   if(thr>0.4):
    if(df['_source.es'][i+1].astype(float) != 0):
     thr2=((df['_source.tp'][i+1].astype(float)-df['_source.tp'][i+2].astype(float))/df['_source.tp'][i+1].astype(float))
     if(thr2>0.2):
      throttleAcc=throttleAcc+1
print('throttle count',throttleAcc)
# disconnect from server
db.close()

